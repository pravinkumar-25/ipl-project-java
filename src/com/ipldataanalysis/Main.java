package com.ipldataanalysis;

import java.io.*;
import java.util.*;

public class Main {
    private static final int MATCH_ID = 0;
    private static final int SEASON = 1;
    private static final int TEAM_ONE = 4;
    private static final int TEAM_tWO = 5;
    private static final int TOSS_WINNER = 6;
    private static final int TOSS_DECISION = 7;
    private static final int RESULT = 9;
    private static final int WINNER = 10;
    private static final int BATTING_TEAM = 2;
    private static final int BOWLING_TEAM = 3;
    private static final int OVER = 4;
    private static final int BALL = 5;
    private static final int BATSMAN = 6;
    private static final int BOWLER = 8;
    private static final int WIDE_RUNS = 10;
    private static final int NO_BALL_RUNS = 13;
    private static final int BATSMAN_RUNS = 15;
    private static final int EXTRA_RUNS = 16;
    private static final int TOTAL_RUNS = 17;
    private static final int PLAYER_DISMISSED = 18;
    private static final int DISMISSAL_KIND = 19;
    private static final ArrayList<Match> matches = new ArrayList<>();
    private static final ArrayList<Delivery> deliveries = new ArrayList<>();

    public static void main(String[] args) {
        try {
            BufferedReader br = new BufferedReader(new FileReader("./src/matches.csv"));
            br.readLine();
            while (true) {
                String fileLine = br.readLine();
                if (fileLine == null) {
                    break;
                }
                setMatchesInfo(fileLine);
            }
            br.close();
            br = new BufferedReader(new FileReader("./src/deliveries.csv"));
            br.readLine();
            while (true) {
                String fileLine = br.readLine();
                if (fileLine == null) {
                    break;
                }
                setDeliveriesInfo(fileLine);
            }
            br.close();
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
            e.printStackTrace();
        }

        findNumberOfMatchesPlayedPerYear(matches);
        findNumberOfMatchesWonPerTeam(matches);
        findExtraRunsConcededPerTeam(matches, deliveries);
        findTopTenEconomicBowlers(matches, deliveries);
        findTopTenNumberOfSixes(deliveries);
        findNumberOfTimesTossWonByTeams(matches);
        findTopTenNumberOfWicketsByBowler(matches, deliveries);
        findStrikeRateOfBatsman(matches, deliveries);
        findTeamsWonBothTossAndMatch(matches);
        findWideRunsGivenByEachTeam(matches, deliveries);
        findMostMaidenOversByBowlers(matches, deliveries);
    }

    public static void setMatchesInfo(String matchLine) {
        Match match = new Match();
        String[] matchData = matchLine.split(",");
        match.setMatchId(Integer.parseInt(matchData[MATCH_ID]));
        match.setSeason(matchData[SEASON]);
        match.setTeamOne(matchData[TEAM_ONE]);
        match.setTeamTwo(matchData[TEAM_tWO]);
        match.setTossWinner(matchData[TOSS_WINNER]);
        match.setTossDecision(matchData[TOSS_DECISION]);
        match.setResult(matchData[RESULT]);
        match.setWinner(matchData[WINNER]);
        matches.add(match);
    }

    public static void setDeliveriesInfo(String deliveriesLine) {
        Delivery delivery = new Delivery();
        String[] deliveryData = deliveriesLine.split(",");
        delivery.setMatchId(Integer.parseInt(deliveryData[MATCH_ID]));
        delivery.setBattingTeam(deliveryData[BATTING_TEAM]);
        delivery.setBowlingTeam(deliveryData[BOWLING_TEAM]);
        delivery.setOver(Integer.parseInt(deliveryData[OVER]));
        delivery.setBall(Integer.parseInt(deliveryData[BALL]));
        delivery.setBatsman(deliveryData[BATSMAN]);
        delivery.setBowler(deliveryData[BOWLER]);
        delivery.setWideRuns(Integer.parseInt(deliveryData[WIDE_RUNS]));
        delivery.setNoBallRuns(Integer.parseInt(deliveryData[NO_BALL_RUNS]));
        delivery.setBatsmanRuns(Integer.parseInt(deliveryData[BATSMAN_RUNS]));
        delivery.setExtraRuns(Integer.parseInt(deliveryData[EXTRA_RUNS]));
        delivery.setTotalRuns(Integer.parseInt(deliveryData[TOTAL_RUNS]));
        if (deliveryData.length > 18) {
            delivery.setPlayerDismissed(deliveryData[PLAYER_DISMISSED]);
            delivery.setDismissalKind(deliveryData[DISMISSAL_KIND]);
        } else {
            delivery.setPlayerDismissed(null);
            delivery.setDismissalKind(null);
        }
        deliveries.add(delivery);
    }

    public static void findNumberOfMatchesPlayedPerYear(ArrayList<Match> matches) {
        Map<String, Integer> matchesPerYear = new TreeMap<>();
        for (Match match : matches) {
            if (matchesPerYear.containsKey(match.getSeason())) {
                int matchesPlayed = matchesPerYear.get(match.getSeason());
                matchesPerYear.put(match.getSeason(), matchesPlayed + 1);
            } else {
                matchesPerYear.put(match.getSeason(), 1);
            }
        }
        System.out.println("1. Number of matches played per year of all the years in IPL :");
        System.out.println(matchesPerYear + "\n");
    }

    public static void findNumberOfMatchesWonPerTeam(ArrayList<Match> matches) {
        HashMap<String, Integer> matchesWonPerTeam = new HashMap<>();
        for (Match match : matches) {
            if (matchesWonPerTeam.containsKey(match.getWinner()) && !match.getResult().equals("tie")) {
                int matchesWon = matchesWonPerTeam.get(match.getWinner());
                matchesWonPerTeam.put(match.getWinner(), matchesWon + 1);
            } else if (!match.getWinner().isEmpty() && !match.getWinner().equals("tie")) {
                matchesWonPerTeam.put(match.getWinner(), 1);
            }
        }
        System.out.println("2. Number of matches won of all teams over all the years of IPL :");
        System.out.println(matchesWonPerTeam + "\n");
    }

    public static void findExtraRunsConcededPerTeam(ArrayList<Match> matches, ArrayList<Delivery> deliveries) {
        HashMap<String, Integer> extraRunsByTeam = new HashMap<>();
        String season = "2016";
        for (Match match : matches) {
            if (match.getSeason().equals(season)) {
                for (Delivery delivery : deliveries) {
                    if (delivery.getMatchId() == match.getMatchId()) {
                        if (extraRunsByTeam.containsKey(delivery.getBowlingTeam())) {
                            int extraRuns = delivery.getExtraRuns();
                            int getRuns = extraRunsByTeam.get(delivery.getBowlingTeam());
                            extraRunsByTeam.put(delivery.getBowlingTeam(), extraRuns + getRuns);
                        } else {
                            extraRunsByTeam.put(delivery.getBowlingTeam(), delivery.getExtraRuns());
                        }
                    }
                }

            }
        }
        System.out.println("3. Extra runs conceded by a team in the year " + season + " :");
        System.out.println(extraRunsByTeam + "\n");
    }

    public static void findTopTenEconomicBowlers(ArrayList<Match> matches, ArrayList<Delivery> deliveries) {
        HashMap<String, HashMap<String, Float>> economicBowlers = new HashMap<>();
        HashMap<String, Float> economyOfBowlers = new HashMap<>();
        Map<String, Float> sortedEconomicBowlers = new LinkedHashMap<>();
        for (Match match : matches) {
            if (match.getSeason().equals("2015")) {
                for (int index = 0; index < deliveries.size(); index++) {
                    if (deliveries.get(index).getMatchId() == match.getMatchId()) {
                        if (economicBowlers.containsKey(deliveries.get(index).getBowler())) {
                            if (index + 1 != deliveries.size() && deliveries.get(index + 1).getOver() != deliveries.get(index).getOver()) {
                                if (deliveries.get(index).getBall() >= 6) {
                                    float over = economicBowlers.get(deliveries.get(index).getBowler()).get("overs") + 1.0f;
                                    economicBowlers.get(deliveries.get(index).getBowler()).put("overs", over);
                                } else {
                                    float over = (float) (deliveries.get(index).getBall()) / 6;
                                    float getOvers = economicBowlers.get(deliveries.get(index).getBowler()).get("overs");
                                    economicBowlers.get(deliveries.get(index).getBowler()).put("overs", over + getOvers);
                                }
                            } else if (index + 1 == deliveries.size()) {
                                if (deliveries.get(index).getBall() >= 6) {
                                    float over = economicBowlers.get(deliveries.get(index).getBowler()).get("overs") + 1.0f;
                                    economicBowlers.get(deliveries.get(index).getBowler()).put("overs", over);
                                } else {
                                    float over = (float) (deliveries.get(index).getBall()) / 6;
                                    float getOvers = economicBowlers.get(deliveries.get(index).getBowler()).get("overs");
                                    economicBowlers.get(deliveries.get(index).getBowler()).put("overs", over + getOvers);
                                }
                            }
                            float runs = economicBowlers.get(deliveries.get(index).getBowler()).get("runs") + (deliveries.get(index).getTotalRuns());
                            economicBowlers.get(deliveries.get(index).getBowler()).put("runs", runs);
                        } else {
                            HashMap<String, Float> runsAndOvers = new HashMap<>();
                            runsAndOvers.put("runs", (float) deliveries.get(index).getTotalRuns());
                            if (index + 1 != deliveries.size() && deliveries.get(index + 1).getOver() != deliveries.get(index).getOver()) {
                                if (deliveries.get(index).getBall() >= 6) {
                                    runsAndOvers.put("overs", 1.0f);
                                } else {
                                    float over = (float) (deliveries.get(index).getBall()) / 6;
                                    runsAndOvers.put("overs", over);
                                }
                            } else if (index + 1 == deliveries.size()) {
                                if (deliveries.get(index).getBall() >= 6) {
                                    runsAndOvers.put("overs", 1.0f);
                                } else {
                                    float over = (float) (deliveries.get(index).getBall()) / 6;
                                    runsAndOvers.put("overs", over);
                                }
                            } else {
                                runsAndOvers.put("overs", 0.0f);
                            }
                            economicBowlers.put(deliveries.get(index).getBowler(), runsAndOvers);
                        }
                    }
                }
            }
        }
        economicBowlers.forEach((bowler, runsAndOvers) -> {
            float economy = (float) runsAndOvers.get("runs") / runsAndOvers.get("overs");
            economyOfBowlers.put(bowler, economy);
        });
        List<Map.Entry<String, Float>> economicList = new LinkedList<>(economyOfBowlers.entrySet());
        economicList.sort((firstValue, secondValue) -> firstValue.getValue().compareTo(secondValue.getValue()));
        for (Map.Entry<String, Float> economy : economicList) {
            sortedEconomicBowlers.put(economy.getKey(), economy.getValue());
            if (sortedEconomicBowlers.size() == 10) {
                break;
            }
        }
        System.out.println("4. Top 10 most economic players in the season 2015 :");
        System.out.println(sortedEconomicBowlers + "\n");
    }

    public static void findTopTenNumberOfSixes(ArrayList<Delivery> deliveries) {
        Map<String, Integer> totalSixes = new HashMap<>();
        Map<String, Integer> topTenSixesCount = new LinkedHashMap<>();
        for (Delivery delivery : deliveries) {
            if (delivery.getBatsmanRuns() == 6) {
                if (totalSixes.containsKey(delivery.getBatsman())) {
                    totalSixes.put(delivery.getBatsman(), totalSixes.get(delivery.getBatsman()) + 1);
                } else {
                    totalSixes.put(delivery.getBatsman(), 1);
                }
            }
        }
        List<Map.Entry<String, Integer>> sixesList = new LinkedList<>(totalSixes.entrySet());
        sixesList.sort((firstValue, secondValue) -> secondValue.getValue().compareTo(firstValue.getValue()));
        for (Map.Entry<String, Integer> sixes : sixesList) {
            topTenSixesCount.put(sixes.getKey(), sixes.getValue());
            if (topTenSixesCount.size() == 10) {
                break;
            }
        }
        System.out.println("5. Top 10 batsmen with highest number of sixes in all seasons :");
        System.out.println(topTenSixesCount + "\n");
    }

    public static void findNumberOfTimesTossWonByTeams(ArrayList<Match> matches) {
        HashMap<String, Integer> wonToss = new HashMap<>();
        for (Match match : matches) {
            if (wonToss.containsKey(match.getTossWinner())) {
                wonToss.put(match.getTossWinner(), wonToss.get(match.getTossWinner()) + 1);
            } else {
                wonToss.put(match.getTossWinner(), 1);
            }
        }
        System.out.println("6. Number of times a team won toss in all seasons :");
        System.out.println(wonToss + "\n");
    }

    public static void findTopTenNumberOfWicketsByBowler(ArrayList<Match> matches, ArrayList<Delivery> deliveries) {
        HashMap<String, Integer> wickets = new HashMap<>();
        Map<String, Integer> topTenWickets = new LinkedHashMap<>();
        for (Match match : matches) {
            if (match.getSeason().equals("2010")) {
                for (Delivery delivery : deliveries) {
                    if (delivery.getMatchId() == match.getMatchId() && delivery.getDismissalKind() != null) {
                        if (wickets.containsKey(delivery.getBowler())) {
                            wickets.put(delivery.getBowler(), wickets.get(delivery.getBowler()) + 1);
                        } else {
                            wickets.put(delivery.getBowler(), 1);
                        }
                    }
                }
            }
        }
        List<Map.Entry<String, Integer>> wicketsList = new LinkedList<>(wickets.entrySet());
        wicketsList.sort((firstValue, secondValue) -> secondValue.getValue().compareTo(firstValue.getValue()));
        for (Map.Entry<String, Integer> wicketList : wicketsList) {
            topTenWickets.put(wicketList.getKey(), wicketList.getValue());
            if (topTenWickets.size() == 10) {
                break;
            }
        }
        System.out.println("7. Top 10 bowlers who took highest number of wickets in the season 2010 :");
        System.out.println(topTenWickets + "\n");
    }

    public static void findStrikeRateOfBatsman(ArrayList<Match> matches, ArrayList<Delivery> deliveries) {
        HashMap<String, HashMap<String, Integer>> strikeDetails = new HashMap<>();
        HashMap<String, Float> strikeRate = new HashMap<>();
        Map<String, Float> topStrikeRatePlayers = new LinkedHashMap<>();
        for (Match match : matches) {
            if (match.getSeason().equals("2011")) {
                for (Delivery delivery : deliveries) {
                    if (match.getMatchId() == delivery.getMatchId()) {
                        if (strikeDetails.containsKey(delivery.getBatsman())) {
                            int balls = strikeDetails.get(delivery.getBatsman()).get("balls") + 1;
                            int runs = strikeDetails.get(delivery.getBatsman()).get("runs") + (delivery.getBatsmanRuns());
                            if (delivery.getNoBallRuns() == 0 && delivery.getWideRuns() == 0) {
                                strikeDetails.get(delivery.getBatsman()).put("balls", balls);
                            }
                            strikeDetails.get(delivery.getBatsman()).put("runs", runs);
                        } else {
                            HashMap<String, Integer> runsAndBalls = new HashMap<>();
                            runsAndBalls.put("runs", delivery.getBatsmanRuns());
                            if (delivery.getNoBallRuns() == 0 && delivery.getWideRuns() == 0) {
                                runsAndBalls.put("balls", 1);
                            } else {
                                runsAndBalls.put("balls", 0);
                            }
                            strikeDetails.put(delivery.getBatsman(), runsAndBalls);
                        }
                    }
                }
            }
        }
        strikeDetails.forEach((batsman, runsAndBalls) -> {
            float strike = (float) runsAndBalls.get("runs") / runsAndBalls.get("balls");
            strike *= 100;
            strikeRate.put(batsman, strike);
        });
        List<Map.Entry<String, Float>> strikeRateList = new LinkedList<>(strikeRate.entrySet());
        strikeRateList.sort((firstValue, secondValue) -> secondValue.getValue().compareTo(firstValue.getValue()));
        for (Map.Entry<String, Float> strikeRateByPlayer : strikeRateList) {
            topStrikeRatePlayers.put(strikeRateByPlayer.getKey(), strikeRateByPlayer.getValue());
            if (topStrikeRatePlayers.size() == 10) {
                break;
            }
        }
        System.out.println("8. Top 10 batsmen with highest strike rate in the season 2011 :");
        System.out.println(topStrikeRatePlayers + "\n");
    }

    public static void findTeamsWonBothTossAndMatch(ArrayList<Match> matches) {
        String SEASON = "2012";
        HashMap<String, Integer> wonBothTossAndMatch = new HashMap<>();
        for (Match match : matches) {
            if (match.getSeason().equals(SEASON) && match.getTossWinner().equals(match.getWinner())) {
                if (wonBothTossAndMatch.containsKey(match.getTossWinner())) {
                    wonBothTossAndMatch.put(match.getTossWinner(), wonBothTossAndMatch.get(match.getTossWinner()) + 1);
                } else {
                    wonBothTossAndMatch.put(match.getTossWinner(), 1);
                }
            }
        }
        System.out.println("9. Number of times a team won both toss and match in the season " + SEASON + " :");
        System.out.println(wonBothTossAndMatch + "\n");
    }

    public static void findWideRunsGivenByEachTeam(ArrayList<Match> matches, ArrayList<Delivery> deliveries) {
        String SEASON = "2015";
        HashMap<String, Integer> wideRunsByTeams = new HashMap<>();
        for (Match match : matches) {
            if (match.getSeason().equals(SEASON)) {
                for (Delivery delivery : deliveries) {
                    if (match.getMatchId() == delivery.getMatchId() && delivery.getWideRuns() != 0) {
                        if (wideRunsByTeams.containsKey(delivery.getBowlingTeam())) {
                            wideRunsByTeams.put(delivery.getBowlingTeam(), wideRunsByTeams.get(delivery.getBowlingTeam()) + (delivery.getWideRuns()));
                        } else {
                            wideRunsByTeams.put(delivery.getBowlingTeam(), delivery.getWideRuns());
                        }
                    }
                }
            }
        }
        System.out.println("10. Total wide runs given by a team in the season " + SEASON + " :");
        System.out.println(wideRunsByTeams + "\n");
    }

    public static void findMostMaidenOversByBowlers(ArrayList<Match> matches, ArrayList<Delivery> deliveries) {
        HashMap<String, Integer> numberOfMaidenOvers = new HashMap<>();
        Map<String, Integer> topTenBowlersWithMostMaidenOvers = new LinkedHashMap<>();
        String SEASON = "2009";
        int runsPerOver = 0;
        for (Match match : matches) {
            if (match.getSeason().equals(SEASON)) {
                for (int index = 0; index < deliveries.size(); index++) {
                    if (deliveries.get(index).getMatchId() == match.getMatchId()) {
                        runsPerOver += deliveries.get(index).getTotalRuns();
                        if (index + 1 == deliveries.size() || deliveries.get(index + 1).getBall() == 1) {
                            if (runsPerOver == 0) {
                                if (numberOfMaidenOvers.containsKey(deliveries.get(index).getBowler())) {
                                    int getMaidenOvers = numberOfMaidenOvers.get(deliveries.get(index).getBowler());
                                    numberOfMaidenOvers.put(deliveries.get(index).getBowler(), getMaidenOvers + 1);
                                } else {
                                    numberOfMaidenOvers.put(deliveries.get(index).getBowler(), 1);
                                }
                            }
                            runsPerOver = 0;
                        }

                    }
                }
            }
        }
        List<Map.Entry<String, Integer>> maidenOversList = new LinkedList<>(numberOfMaidenOvers.entrySet());
        maidenOversList.sort((firstValue, secondValue) -> secondValue.getValue().compareTo(firstValue.getValue()));
        for (Map.Entry<String, Integer> bowlers : maidenOversList) {
            topTenBowlersWithMostMaidenOvers.put(bowlers.getKey(), bowlers.getValue());
            if (topTenBowlersWithMostMaidenOvers.size() == 10) {
                break;
            }
        }
        System.out.println("11. Top ten bowlers with most maiden overs in the season 2009 :");
        System.out.println(topTenBowlersWithMostMaidenOvers + "\n");
    }
}
